#include "Capture.h"


void show(cv::Mat img) {
	cv::imshow("image", img);
	cv::waitKey(15);
}


Capture::Capture(const char *img_path) {
	std::cout << "Loading image from " << img_path << std::endl;
	frame = cv::imread(img_path);
	w = frame.cols;
	h = frame.rows;
}


cv::Mat Capture::kernel_patch_gen(int feature_map_idx) {
	int fmap_row_idx = int(feature_map_idx / feature_map_width);
	stencil_bboxes_tlbrxy[feature_map_idx][0] = (feature_map_idx - (fmap_row_idx * feature_map_width)) * kernel_stride;
	stencil_bboxes_tlbrxy[feature_map_idx][1] = fmap_row_idx * kernel_stride;	
	stencil_bboxes_tlbrxy[feature_map_idx][2] = stencil_bboxes_tlbrxy[feature_map_idx][0] + kernel_size;
	stencil_bboxes_tlbrxy[feature_map_idx][3] = stencil_bboxes_tlbrxy[feature_map_idx][1] + kernel_size;
	cv::Mat stencil_mat = frame(Rect(stencil_bboxes_tlbrxy[feature_map_idx][0],
		stencil_bboxes_tlbrxy[feature_map_idx][1], kernel_size, kernel_size));
	feature_map_idx++;
	return stencil_mat;
}


int Capture::get_num_strides(int dim_size, int filter_size, int stride) {
	int num_strides = ((dim_size - filter_size) / stride) + 1;
	return num_strides;
}


void Capture::ExtractImagePatches(int kernel_window_side, int stride) {
	kernel_size = kernel_window_side;
	kernel_stride = stride;
	feature_map_width = get_num_strides(w, kernel_size, stride);
	feature_map_height = get_num_strides(h, kernel_size, stride);
	int num_feature_maps = feature_map_width * feature_map_height;
	feature_map_receptive_fields.clear();
	stencil_bboxes_tlbrxy.clear();
	feature_map_receptive_fields.resize(num_feature_maps);
	stencil_bboxes_tlbrxy.resize(num_feature_maps, {0, 0, 0, 0});

#pragma omp for
	for (int i = 0; i < num_feature_maps; i++) {
		cv::Mat kernel_patch = kernel_patch_gen(i);
		feature_map_receptive_fields[i] = kernel_patch;
	}
}

void Capture::ExtractPRFeatures(int kernel_window_side, int stride) {
	ExtractImagePatches(kernel_window_side, stride);
	for (auto const& patch_mat : feature_map_receptive_fields) {
		PRFeature pixrand_feature(patch_mat);
		pixrand_features.push_back(pixrand_feature);
	}
}
