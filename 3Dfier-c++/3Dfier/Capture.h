#pragma once

#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>

#include <omp.h>

#include "PRFeature.h"


using namespace std;
using namespace cv;


class Capture {
public:
	cv::Mat frame;
	std::vector<PRFeature> pixrand_features;
	int w, h;
	int feature_map_width, feature_map_height;
	std::vector<std::vector<int>> stencil_bboxes_tlbrxy;
	std::vector<cv::Mat> feature_map_receptive_fields;

	Capture(const char *img_path);

	void ExtractImagePatches(int kernel_window_side, int side);
	void ExtractPRFeatures(int kernel_window_side, int stride); // PR -> PixRand :D

private:
	int kernel_size, kernel_stride;
	cv::Mat kernel_patch_gen(int feature_map_idx);
	int get_num_strides(int dim_size, int filter_size, int stride);
};

