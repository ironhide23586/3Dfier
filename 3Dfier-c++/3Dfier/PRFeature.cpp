#include "PRFeature.h"


std::string type2str(int type) {
	string r;

	uchar depth = type & CV_MAT_DEPTH_MASK;
	uchar chans = 1 + (type >> CV_CN_SHIFT);

	switch (depth) {
	case CV_8U:  r = "8U"; break;
	case CV_8S:  r = "8S"; break;
	case CV_16U: r = "16U"; break;
	case CV_16S: r = "16S"; break;
	case CV_32S: r = "32S"; break;
	case CV_32F: r = "32F"; break;
	case CV_64F: r = "64F"; break;
	default:     r = "User"; break;
	}

	r += "C";
	r += (chans + '0');

	return r;
}


PRFeature::PRFeature(cv::Mat image_patch) {
	w = image_patch.cols;
	h = image_patch.rows;
	c = 1 + (image_patch.type() >> CV_CN_SHIFT);
	num_pixels = w * h;
	//std::string tp = type2str(image_patch.type());
	int i, j;

	std::vector<float> channel(num_pixels);

	for (j = 0; j < c; j++) {
		feature_vect.push_back(channel);
	}
	channel.clear();

	std::vector<int> indices(num_pixels);
	for (i = 0; i < num_pixels; i++) {
		indices[i] = i;
	}
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(indices.begin(), indices.end(), std::default_random_engine(seed));

#pragma omp for
	for (i = 0; i < num_pixels; i++) {
		for (j = 0; j < c; j++) {
			feature_vect[j][indices[i]] = (float) image_patch.data[indices[i] * c + j] / 255.0;
		}
	}

	//TODO: Compute means, stds and normalize shuffled pixels - SB 25/09/2019


}
