#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include <cstdlib>

#include <opencv2/opencv.hpp>
#include <omp.h>
//#include <boost/multi_array.hpp>


using namespace std;
using namespace cv;


class PRFeature {
public:

	int w, h, c, num_pixels;
	std::vector<float> channelwise_means;
	std::vector<float> channelwise_stds;

	std::vector<std::vector<float>> feature_vect;

	PRFeature(cv::Mat image_patch);
};

